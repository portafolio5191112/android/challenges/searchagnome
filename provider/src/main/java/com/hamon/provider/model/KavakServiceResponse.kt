package com.hamon.provider.model

import com.google.gson.annotations.SerializedName

data class KavakServiceResponse(@SerializedName("Brastlewark") val response: MutableList<Population> = mutableListOf())
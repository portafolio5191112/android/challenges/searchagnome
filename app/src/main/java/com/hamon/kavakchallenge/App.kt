package com.hamon.kavakchallenge

import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.hamon.kavakchallenge.di.Modules
import timber.log.Timber

class App : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        Modules.init(this)
    }
}
package com.hamon.kavakchallenge.base

interface ILogBase {
    fun logDebug(message: String){}
    fun logDebug(tag: String, message: String)
    fun logInfo(message: String)
    fun logInfo(tag: String, message: String)
    fun logWarning(message: String)
    fun logWarning(tag: String, message: String)
    fun logError(message: String)
    fun logError(tag: String, message: String)
}
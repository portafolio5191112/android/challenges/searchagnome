package com.hamon.kavakchallenge.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import timber.log.Timber

open class BaseApplicationViewModel(private  val app: Application): AndroidViewModel(app), ILogBase  {

    //logs functions
    override fun logDebug(message: String) {
        Timber.d(message)
    }

    override fun logDebug(tag: String, message: String) {
        Timber.tag(tag).d(message)
    }

    override fun logInfo(message: String) {
        Timber.i(message)
    }

    override fun logInfo(tag: String, message: String) {
        Timber.tag(tag).i(message)
    }

    override fun logWarning(message: String) {
        Timber.w(message)
    }

    override fun logWarning(tag: String, message: String) {
        Timber.tag(tag).w(message)
    }

    override fun logError(message: String) {
        Timber.e(message)
    }

    override fun logError(tag: String, message: String) {
        Timber.tag(tag).e(message)
    }

}
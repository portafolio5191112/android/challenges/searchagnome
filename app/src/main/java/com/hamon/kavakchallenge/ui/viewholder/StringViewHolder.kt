package com.hamon.kavakchallenge.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.hamon.kavakchallenge.databinding.LayoutItemStringBinding

class StringViewHolder(private val binding: LayoutItemStringBinding) :
    RecyclerView.ViewHolder(binding.root) {

        fun bind(string: String){
            binding.tvString.text = string
        }
}
package com.hamon.kavakchallenge.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.hamon.kavakchallenge.R
import com.hamon.kavakchallenge.actions.NetworkActions
import com.hamon.kavakchallenge.actions.SearchActions
import com.hamon.kavakchallenge.base.BaseFragment
import com.hamon.kavakchallenge.databinding.FragmentSearchBinding
import com.hamon.kavakchallenge.ui.adapter.PopulationAdapter
import com.hamon.kavakchallenge.util.hideKeyboard
import com.hamon.kavakchallenge.viewmodel.MainViewModel
import com.hamon.kavakchallenge.viewmodel.ViewModelFactory
import com.hamon.provider.model.Population
import org.koin.android.ext.android.bind
import org.koin.android.ext.android.inject

const val POPULATION_KEY = "population_selected"

class SearchFragment : BaseFragment() {

    private val binding: FragmentSearchBinding by lazy {
        FragmentSearchBinding.inflate(LayoutInflater.from(context), null, false)
    }
    private val viewModelFactory: ViewModelFactory by inject()
    private val viewModel: MainViewModel by navGraphViewModels(R.id.nav_graph) {
        viewModelFactory
    }
    private val populationAdapter: PopulationAdapter by lazy {
        PopulationAdapter { population ->
            populationSelected(population)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProgressBar()
        setupRecyclerView()
        setupSearchView()
        setupNetworkObservable()
        setupSearchObservable()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        finishParentActivity()
    }

    private fun setupSearchObservable() {
        viewModel.searchActionsObservable.observerInside { event ->
            when (event) {
                is SearchActions.HasData -> showData(event.productList)
                is SearchActions.HasError -> showError(event.message)
                is SearchActions.EmptySearch -> showEmptyList()
            }
        }
    }

    private fun setupSearchView() {
        binding.svProduct.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextSubmit(query: String?): Boolean {
                hideKeyboard()
                if (!query.isNullOrEmpty()) {
                    viewModel.apply {
                        setQuery(query)
                        context?.let { isNetworkConnected() }
                    }
                    showProgressBar()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean = false

        })

    }

    private fun populationSelected(population: Population) {
        showProgressBar()
        viewModel.populationSelected(population)
        findNavController().navigate(R.id.fragment_detail_search)
    }

    private fun showData(populationList: MutableList<Population>) {
        binding.rvPopulation.visibility = View.VISIBLE
        hideInformativeScreen()
        populationAdapter.submitList(populationList)
        hideProgressBar()
    }

    private fun showError(message: String) {
        showSnackBarError(message = message)
    }

    private fun setupNetworkObservable() {
        viewModel.networkActionsObservable.observerInside { event ->
            when (event) {
                is NetworkActions.NetworkOK -> applyQuery()
                is NetworkActions.NetworkError -> showProblemNetwork()
            }
        }
    }

    private fun applyQuery() {
        binding.rvPopulation.visibility = View.GONE
        viewModel.searchPopulation()
    }

    private fun setupRecyclerView() {
        binding.rvPopulation.adapter = populationAdapter
    }

    private fun showProblemNetwork() {
        binding.rvPopulation.visibility = View.GONE
        hideProgressBar()
        binding.apply {
            context?.let {
                ivInformativeIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.ic_no_wifi
                    )
                )
            }
            tvInformativeText.text = getString(R.string.network_error)
            cInformativeScreen.visibility = View.VISIBLE
        }
        viewModel.restartNetworkEvent()
    }

    private fun showEmptyList() {
        binding.rvPopulation.visibility = View.GONE
        hideProgressBar()
        binding.apply {
            context?.let {
                ivInformativeIcon.setImageDrawable(
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.ic_to_do_list
                    )
                )
            }
            tvInformativeText.text = getString(R.string.empty_search_population)
            cInformativeScreen.visibility = View.VISIBLE
        }
    }

    private fun hideInformativeScreen() {
        binding.cInformativeScreen.visibility = View.GONE
    }

}
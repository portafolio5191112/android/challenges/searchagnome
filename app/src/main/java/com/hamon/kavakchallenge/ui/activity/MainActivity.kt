package com.hamon.kavakchallenge.ui.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.hamon.kavakchallenge.base.BaseActivity
import com.hamon.kavakchallenge.databinding.MainActivityBinding

class MainActivity : BaseActivity() {

    private val binding: MainActivityBinding by lazy {
        MainActivityBinding.inflate(LayoutInflater.from(this))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

    fun showSnackBarError(msg: String) {
        Snackbar.make(binding.root, msg, Snackbar.LENGTH_LONG)
            .setBackgroundTint(ContextCompat.getColor(this, android.R.color.holo_red_light))
            .setTextColor(ContextCompat.getColor(this, android.R.color.white))
            .show()
    }

    fun showSnackBarSuccess(msg: String) {
        Snackbar.make(binding.root, msg, Snackbar.LENGTH_LONG)
            .setBackgroundTint(ContextCompat.getColor(this, android.R.color.holo_green_dark))
            .setTextColor(ContextCompat.getColor(this, android.R.color.white))
            .show()
    }

    fun showProgressCard() {
        binding.progressCard.visibility = View.VISIBLE
    }

    fun hideProgressCard() {
        binding.progressCard.visibility = View.GONE
    }

}
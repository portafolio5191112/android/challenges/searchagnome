package com.hamon.kavakchallenge.di

import android.app.Application
import com.hamon.kavakchallenge.viewmodel.MainViewModel
import com.hamon.kavakchallenge.viewmodel.ViewModelFactory
import com.hamon.provider.controller.GetListPopulationUseCase
import com.hamon.provider.controller.GetListPopulationUseCaseImpl
import com.hamon.provider.repository.RemoteDataSource
import com.hamon.provider.repository.RemoteRepository
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

object Modules {

    fun init(application: Application) {
        startKoin {
            androidLogger()
            androidContext(application)
            androidFileProperties()
            koin.loadModules(
                arrayListOf(
                    appModules
                )
            )
            koin.createRootScope()
        }
    }


    private val appModules = module {
        factory<RemoteDataSource> { RemoteRepository() }
        single { GetListPopulationUseCaseImpl(get()) }
        viewModel { MainViewModel(get(), get()) }
        factory { ViewModelFactory(get(), get()) }
    }

}